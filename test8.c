#include<stdio.h>
int main()
{
    char str[100],rev[100];
    int i,l=0,c=0,flag;
    printf("Enter a string \n");
    scanf("%s",str);
    for(i=0;str[i]!='\0';i++)
        l++;
    for(i=l-1;i>=0;i--)
    {
        rev[c]=str[i];
        c++;
    }
    rev[c]='\0';
    for(i=0;i<l;i++)
    {
        if(rev[i]==str[i])
            flag=0;
        else
            flag=1;
    }
    if(flag==0)
        printf("The entered string is a palindrome \n");
    else
        printf("The entered string is not a palindrome \n");
    return 0;
}
