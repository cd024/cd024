#include<stdio.h>
#include<math.h>
int main()
{
	int a,b,c;
	float r1,r2;
	double d,realpart , imagpart;
	printf("Enter the values of a,b,c \n");
	scanf("%d %d %d",&a,&b,&c);
	d = (b*b)-(4*a*c);
	if(d>0)
	{ 
		r1 = (-b + sqrt(d))/(2*a);
		r2 = (-b - sqrt(d))/(2*a);
		printf("Real roots = %f,%f",r1,r2);
	}
	else if(d==0)
	{
		r1 = -b/(2*a);
		printf("Equal roots = %f",r1);
	}
	else
	{
		realpart = -b/(2*a);
		imagpart = sqrt(fabs(d))/(2*a);
		printf("Imaginary roots = %lf+i%lf , %lf-i%lf", realpart,imagpart,realpart,imagpart);
	}
	return 0;
}



