#include<stdio.h>
int main()
{
	int i;
	struct name
	{
	    char firstname[100];
	    char lastname[100];
	};
	struct student
	{
		int roll_no;
		struct name n;
		char section[2];
		char department[50];
		float fees;
		int totalmarks;
	}s[2];
	for(i=0;i<2;i++)
	{
		printf("Enter details of student %d\n",i+1);
		printf("Enter roll no ");
		scanf("%d",&s[i].roll_no);
		printf("Enter name ");
		scanf("%s %s",s[i].n.firstname,s[i].n.lastname);
		printf("Enter section ");
		scanf("%s",s[i].section);
		printf("Enter department ");
		scanf("%s",s[i].department);
		printf("Enter fees ");
		scanf("%f",&s[i].fees);
		printf("Enter total marks ");
		scanf("%d",&s[i].totalmarks);
		printf("\n");
	}
	if(s[0].totalmarks>s[1].totalmarks)
	{
		printf("Student 1 has scored the highest and details are \n");
		printf("Roll no - %d \n",s[0].roll_no);
		printf("Name - %s %s \n",s[0].n.firstname,s[0].n.lastname);
		printf("Section - %s \n",s[0].section);
		printf("Departmental - %s \n",s[0].department);
		printf("Fees - %f \n",s[0].fees);
		printf("Total marks - %d \n",s[0].totalmarks);
	}
	else
	{
		printf("Student 1 has scored the highest and details are \n");
		printf("Roll no - %d \n",s[1].roll_no);
		printf("Name - %s %s \n",s[1].n.firstname,s[1].n.lastname);
		printf("Section - %s \n",s[1].section);
		printf("Departmental - %s \n",s[1].department);
		printf("Fees - %f \n",s[1].fees);
		printf("Total marks - %d \n",s[1].totalmarks);
	}
	return 0;
}
