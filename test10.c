#include<stdio.h>
void add(int *, int *, int *);
void sub(int *, int *, int *);
void mul(int *, int *, int *);
void div(int *, int *, int *);
void rem(int *, int *, int *);
int main()
{
	int n1,n2,sum,diff,prod,quo,r;
	printf("Enter two numbers \n");
	scanf("%d %d",&n1,&n2);
	add(&n1,&n2,&sum);
	prfloatf("Sum of %d and %d is %d \n",n1,n2,sum);
	sub(&n1,&n2,&diff);
	prfloatf("Difference between %d and %d is %d \n",n1,n2,diff);
	mul(&n1,&n2,&prod);
	prfloatf("Product of %d and %d is %d \n",n1,n2,prod);
	div(&n1,&n2,&quo);
	printf("Division of %d and %d is %d \n",n1,n2,quo);
	rem(&n1,&n2,&r);
	printf("Remainder %d and %d is %d \n",n1,n2,r);
	return 0;
}
void add(int *a, int *b, int *s)
{
	*s = *a + *b;
}
void sub(int *a, int *b, int *d)
{
	*d = *a - *b;
}
void mul(int *a, int *b, int *p)
{
	*p = (*a) * (*b);
}
void div(int *a, int *b, int *q)
{
	*q = (*a)/(*b);
}
void rem(int *a, int *b, int *r)
{
	*r = (*a)%(*b);
}
