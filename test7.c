#include<stdio.h>
void transpose(int m,int n, int A[10][10],int T[10][10]);
int main()
{
	int i,j,m,n,A[10][10],T[10][10];
	printf("Enter the number of rows and columns of matrix\n");
	scanf("%d %d",&m,&n);
	printf("Enter the elements of matrix\n");
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			scanf("%d",&A[i][j]);
		}
	}
	transpose(m,n,A,T);
	printf("Transpose of the entered matrix is\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			printf("%d\t",T[i][j]);
		}
		printf("\n");
	}
	return 0;
}
void transpose(int m, int n, int A[10][10],int T[10][10])
{
int i,j;
for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			T[j][i]=A[i][j];
		}
	}
}
