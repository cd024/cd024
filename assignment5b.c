#include<stdio.h>
int main()
{
    int i,n,pos;
    int odd[10] = {1,3,5,7,9,11,13,15};
    pos = 2;
    n=8;
    printf("Array before deleting 5 is\n");
    for(i=0;i<n;i++)
    {
        printf("%d ",odd[i]);
    }
    printf("\n");
    for(i=pos;i<n;i++)
    {
        odd[i] = odd[i+1];
    }
    n = n-1;
    printf("Array after deleting 5 is\n");
    for(i=0;i<n;i++)
    {
        printf("%d " ,odd[i]);
    }
    return 0;
}
