#include<stdio.h>
#include<math.h>
int main()
{
	int a,b,c,flag;
	double d,r1,r2,realpart,imagpart;
	printf("Enter the values of a,b,c \n");
	scanf("%d %d %d" , &a,&b,&c);
	d = (b*b)-(4*a*c);
	if(d>0)
		flag = 1;
	else if(d==0)
		flag = 2;
	else 
		flag = 3;
	switch(flag)
	{
		case 1: r1 = (-b + sqrt(d))/(2*a);
			r2 = (-b - sqrt(d))/(2*a);
			printf("Real roots = %lf,%lf",r1,r2);
			break;
		case 2 : r1 = -b/(2*a);
			printf("Equal roots = %lf",r1);
			break;
		case 3 : realpart = -b/(2*a);
			imagpart = sqrt(fabs(d))/(2*a);
			printf("Imaginary roots = %lf+i%lf , %lf-i%lf", realpart,imagpart,realpart,imagpart);
			break;
	}
	return 0;
}
