//Binary Search is the searching technique.
#include<stdio.h>
int main()
{
	int n=6,pos=-1;
	int A[20] = {2,3,5,7,11,13};
	int SE = 11;
	int low=0,high=n-1,mid;
	while(low<=high)
	{
		mid=(low+high)/2;
		if(A[mid]<SE)
			low=mid+1;
		else if(A[mid]==SE)
			{
			    pos=mid;
			    break;
			}
		else
			high=mid-1;
	}
	printf("%d is present at the index %d",SE,pos);
	return 0;
}
