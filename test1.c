#include<stdio.h>
float input()
{
	float a;
	scanf("%f",&a);
	return a;
}
float compute(float P ,float T ,float R)
{
	float SI;
	SI = (P*T*R)/100;
	return SI;
}
void display(float SI)
{
	printf("Simple Interest is %f" , SI);
}
int main()
{ 
	float P , T , R , SI;
	printf("Enter Principal\n");
	P = input();
	printf("Enter Time\n");
	T = input();
	printf("Enter Rate\n");
	R = input();
	SI = compute(P,T,R);
	display(SI);	
	return 0;
}
