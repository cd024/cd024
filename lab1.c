#include<stdio.h>
#include<math.h>
int main()
{
	float x1 , x2 , y1 , y2 , D;
	printf("Enter two co-ordinates \n");
	scanf("(%f,%f) (%f,%f)" , &x1 , &y1 , &x2 , &y2);
	D = sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	printf("Distance between (%f , %f) and (%f, %f) is %f" , x1 , y1 , x2 , y2, D);
	return 0;
}