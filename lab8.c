#include<stdio.h>
int main()
{
	struct employee
	{
		int emp_id;
		char emp_name[50];
		float salary;
		char DOB[20];
		char company[50];
	}e1;
	printf("Enter the employee id \n");
	scanf("%d", &e1.emp_id);
	printf("Enter the employee name \n");
	scanf("%s", e1.emp_name);
	printf("Enter the salary \n");
	scanf("%f", &e1.salary);
	printf("Enter the DOB \n");
	scanf("%s", e1.DOB);
	printf("Enter the company name \n");
	scanf("%s", e1.company);
	printf("\n");
	printf("EMPLOYEE DETAILS: \n");
	printf("Employee id - %d \n",e1.emp_id);
	printf("Name - %s \n",e1.emp_name);
	printf("Salary - %f \n",e1.salary);
	printf("DOB - %s \n",e1.DOB);
	printf("Company - %s \n",e1.company);
	return 0;
}
