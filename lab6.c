#include<stdio.h>
int main()
{
	int M[5][3],i,j,maxM;
	printf("Enter the marks obtained by 5 students in 3 subjects \n");
	for(i=0;i<5;i++)
	{
	    printf("Enter the marks obtained by student %d\n",i+1);
		for(j=0;j<3;j++)
		{
			printf("M[%d][%d] ",i,j);
			scanf("%d",&M[i][j]);
		}
		printf("\n");
	}
	printf("The marks obtained by 5 students in 3 subjects \n");
	for(i=0;i<5;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("\t %d",M[i][j]);
		}
		printf("\n");
	}
	for(j=0;j<3;j++)
	{
		maxM = M[0][j];
		for(i=0;i<5;i++)
		{
			if(M[i][j]>maxM)
				maxM = M[i][j];
		}
		printf("The highest marks obtained in subject %d = %d \n",j+1,maxM);
	}
	return 0;
}

