#include<stdio.h>
#include<math.h>
double input()
{
     double a;
     printf("Enter side of triangle \n");
     scanf("%lf" , &a);
     return a;
}
double compute(double a , double b, double c)
{
     double s = (a+b+c)/2;
     double area = sqrt(s*(s-a)*(s-b)*(s-c));
     return area;
}
void display(double area)
{
     printf("The area of the triangle having above entered sides is %lf" , area);
}
double main()
{
     double a , b , c;
     double areaT;
     a = input();
     b = input();
     c = input();
     if((a>0)&&(b>0)&&(c>0)&&((a+b)>c)&&((b+c)>a)&&((c+a>b)))
     { 
          areaT = compute(a,b,c);
          display(areaT);
     }
     else
          printf("Triangle does not exist");
     return 0;
}




