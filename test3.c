#include<stdio.h>
float input()
{
	float r;
	printf("Enter the radius of circle\n");
	scanf("%f" , &r);
	return r;
}
float compute(float r)
{
	float area ;
	area = 3.14*r*r;
	return area;
}
void display(float area)
{
	printf("Area of circle having above enterd radius is %f" , area);
}
int main()
{
	float r;
	float areaC;
	r = input();
	areaC = compute(r);
	display(areaC);
	return 0;
}
